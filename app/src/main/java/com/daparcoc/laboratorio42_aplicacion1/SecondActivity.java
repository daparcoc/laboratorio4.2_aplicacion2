package com.daparcoc.laboratorio42_aplicacion1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import static android.content.Intent.*;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String varString = getIntent().getStringExtra("valorTest");
        Log.d("HW - Second Activity", varString);
    }
}
